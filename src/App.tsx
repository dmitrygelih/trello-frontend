import React from 'react';
import { connect } from "react-redux";

import Home from './pages/Home'
import Board from './pages/Board'

import { MuiThemeProvider } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import theme from './theme'
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';

import { LOADING_HIDE } from "./store/action-types";

import {
    BrowserRouter,
    Switch,
    Redirect,
    Route
} from "react-router-dom";


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        backdrop: {
            zIndex: theme.zIndex.drawer + 1,
            color: '#fff',
        },
    }),
);

interface AppProps {
    loading: boolean;
    hideLoading: () => void;
}

const App = ({ loading, hideLoading }: AppProps) => {
    const classes = useStyles();

    return (
        <>
            <MuiThemeProvider theme={theme}>
                <CssBaseline />
                <BrowserRouter>
                    <Backdrop className={classes.backdrop} open={loading} onClick={hideLoading}>
                        <CircularProgress color="inherit" />
                    </Backdrop>
                    <Switch>
                        <Route path="/boards/:id" children={<Board />} />
                        <Route path="/" children={<Home />} />
                        <Redirect to='/' />
                    </Switch>
                </BrowserRouter>
            </MuiThemeProvider>
        </>
    );
};

const mapStateToProps = (state: any) => ({ loading: state.pageLoading });

const mapDispatchToProps = (dispatch: any) => ({
    hideLoading: () => {
        dispatch({ type: LOADING_HIDE })
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
