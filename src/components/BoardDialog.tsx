import * as React from "react";
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import Container from '@material-ui/core/Container';

import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import BoardType from '../types/board'

import axios from 'axios';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            marginBottom: theme.spacing(5),
            padding: theme.spacing(0, 5)
        },
    }),
);

interface BoardDialogType {
    title: string
    onCreate: (board: BoardType) => void
}

const BoardDialog = (props: BoardDialogType) => {
    const [open, setOpen] = React.useState(false);
    const [name, setName] = React.useState('');
    const classes = useStyles();

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const create = async () => {
        axios.post('api/boards', { name })
            .then((response) => {
                props.onCreate(response.data.board)
            })
            .finally(() => { setOpen(false) })
    };

    return (
        <Container className={classes.root}>
            <Button variant='outlined' disableElevation onClick={handleClickOpen}>
                Open board dialog
            </Button>
            <Dialog open={open} onClose={handleClose}>
                <DialogTitle id="form-dialog-title">{props.title}</DialogTitle>
                <DialogContent>
                    <TextField autoFocus margin="dense" label="Name" fullWidth onChange={(e) => setName(e.target.value)} />
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        Cancel
                    </Button>
                    <Button onClick={create} color="primary">
                        Create
                    </Button>
                </DialogActions>
            </Dialog>
        </Container>
    );
};

export default BoardDialog
