import React from 'react';

import { daysTillDate } from '../utils/date-utils';
import {DraggingStyle, NotDraggingStyle} from 'react-beautiful-dnd';

import CardType from '../types/card'

import { Draggable } from "react-beautiful-dnd";
import { makeStyles } from '@material-ui/core/styles';

function defineCardColor(daysTillDate: number) {
    if (daysTillDate < 0) {
        return 'red'
    }

    if (daysTillDate === 0) {
        return 'orangered'
    }

    if (daysTillDate === 1) {
        return '#f0d637' // yellow
    }

    return 'white';
}

const useStyles = makeStyles((theme) => ({
    root: {
        backgroundColor: ({card}: CardProps) => {
            if (!card.dueDate) { return 'white' }

            const diff = daysTillDate(new Date(card.dueDate));
            return defineCardColor(diff)
        },
        border: 0,
        borderRadius: theme.shape.borderRadius,
        boxShadow: '0 1px 0 rgba(9,30,66,.25)',
        padding: theme.spacing(3, 4, 1),
        marginBottom: theme.spacing(4),
        overflow: 'hidden',
        position: 'relative',
        zIndex: 10,
        display: 'block',
        textDecoration: 'none',
        maxWidth: 300,
        minHeight: 32,
        wordWrap: 'break-word',
        clear: 'both',
        '&:hover': {
            opacity: 0.8
        },
    },
}));

const getCardStyle = (isDragging:boolean, draggableStyle:DraggingStyle | undefined | NotDraggingStyle) => {
    return {
        ...draggableStyle,
        ...(!isDragging && { cursor: "pointer" })
    }
};

type CardProps = {
    index: number,
    clickHandler: () => void,
    card: CardType
}

function Card(props: CardProps) {
    const classes = useStyles(props);

    return (
        <Draggable draggableId={String(props.card.id)} index={props.index}>
            {(provided, snapshot) => (
                <div className={classes.root}
                     onClick={props.clickHandler}
                     {...provided.draggableProps}
                     {...provided.dragHandleProps}
                     ref={provided.innerRef}
                     style={getCardStyle( snapshot.isDragging, provided.draggableProps.style )}
                >
                    {props.card.name}
                </div>
            )}
        </Draggable>
    )
}

export default Card;
