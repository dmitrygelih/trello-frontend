import React, { useState } from 'react';

import cardActions from "../store/actions/card-actions";
import { connect } from "react-redux";
import AddIcon from '@material-ui/icons/Add';
import { makeStyles } from "@material-ui/core/styles";

import ComposerForm from './ComposerForm'

const useStyles = makeStyles((theme) => ({
    openFormButton: {
        borderRadius: theme.shape.borderRadius,
        color: '#5e6c84',
        padding: theme.spacing(2),
        position: 'relative',
        textDecoration: 'none',
        height: 28,
        userSelect: 'none',
        display: 'flex',
        cursor: 'pointer',
        '&:hover': {
            backgroundColor: 'rgba(9,30,66,.08)',
            color: '#22304C'
        }
    },
}));


interface newCardType {
    name: string,
    stageId: string | number
}

interface CardComposerProps {
    createCard: (params: newCardType) => Promise<any>,
    stageId: number | string,
}

function CardComposer({ createCard, stageId }: CardComposerProps) {
    const [editActive, setEditActive] = useState(false);
    const classes = useStyles();

    const openForm = () => {
        setEditActive(true)
    };

    const submitHandler = (name: string) => {
        if (!name) { return }

        createCard({ name, stageId }).finally(() => {
            setEditActive(false)
        })
    };

    const closeHandler = () => {
        setEditActive(false)
    };

    const formProps = {
        type: "textArea",
        buttonText: 'Add Card',
        close: closeHandler,
        submit: submitHandler,
        placeholder: 'Enter a title for this card...'
    };

    return (
        <div>
            {editActive
                ? <ComposerForm {...formProps} />
                : <span onClick={openForm} className={classes.openFormButton}>
                    <AddIcon fontSize='small'/>Add another card
                    </span>
            }
        </div>
    )
}

const mapDispatchToProps = (dispatch: any) => ({
    createCard: (card: newCardType) => ( dispatch(cardActions.create(card)) )
});

export default connect(null, mapDispatchToProps)(CardComposer);
