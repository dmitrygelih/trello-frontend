import React, { useState } from 'react';
import { createStyles, makeStyles, Theme, withStyles, WithStyles } from '@material-ui/core/styles';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';
import MuiDialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Card from './Card'
import InlineTextEdit from './InlineTextEdit'

import CardType from '../types/card'

import DateFnsUtils from '@date-io/date-fns';

import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker,
} from '@material-ui/pickers';
import {connect} from "react-redux";
import cardActions from "../store/actions/card-actions";

const styles = (theme: Theme) =>
    createStyles({
        root: {
            margin: 0,
            paddingRight: 56
        },
        closeButton: {
            position: 'absolute',
            right: theme.spacing(2),
            top: theme.spacing(2),
        },
    }
);

export interface DialogTitleProps extends WithStyles<typeof styles> {
    id: string;
    children: React.ReactNode;
    onClose: () => void;
}

const DialogTitle = withStyles(styles)((props: DialogTitleProps) => {
    const { children, classes, onClose, ...other } = props;

    return (
        <MuiDialogTitle disableTypography className={classes.root} {...other}>
            {children}
            <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
                <CloseIcon />
            </IconButton>
        </MuiDialogTitle>
    );
});

const DialogContent = withStyles((theme: Theme) => ({
    root: {
        padding: theme.spacing(8),
        '& h4': {
            margin: 0
        }
    },
}))(MuiDialogContent);

const DialogActions = withStyles((theme: Theme) => ({
    root: {
        margin: 0,
        padding: theme.spacing(4),
    },
}))(MuiDialogActions);

const DatePicker = withStyles((theme: Theme) => ({
    root: {
        width: '100%',
        margin: 0
    },
}))(KeyboardDatePicker);

const useStyles = makeStyles((theme) => ({
    dialogHeader: {
    },
    textArr: {
        width: '100%',
        marginTop: 16,
        marginBottom: 24,
        padding: theme.spacing(4, 6),
        backgroundColor: 'rgba(9,30,66,.04)',
        boxShadow: 'none',
        border: 'none',
        resize: 'none',
        borderRadius: theme.shape.borderRadius,
        display: 'block',
        textDecoration: 'none',
        '&:focus': {
            borderColor: '#80bdff',
            backgroundColor: 'white',
            outline: 'none',
            boxShadow: 'inset 0 0 0 2px #0079bf'
        },
    }
}));

type CardDialogProps = {
    index: number,
    key: number | string,
    card: CardType,
    updateCard: (card: CardType) => Promise<any>
}

function CardDialog({ index, card, updateCard }: CardDialogProps) {
    const [open, setOpen] = useState(false);
    const [name, setName] = useState(card.name);
    const [description, setDescription] = useState(card.description || '');
    const [dueDate, setDueDate] = useState(card.dueDate);
    const classes = useStyles();

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setName(card.name);
        setDescription(card.description || '');
        setDueDate(card.dueDate);

        setOpen(false);
    };

    const handleDateChange:any = function(date: Date | undefined) {
        setDueDate(date);
    };

    const saveChanges = () => {
        updateCard({
            id: card.id,
            stageId: card.stageId,
            name: name,
            description: description,
            dueDate: dueDate,
        }).then(() => {
            setOpen(false)
        })
    };

    const handleDescriptionChange:any = (event: React.ChangeEvent<HTMLInputElement>) => {
        setDescription(event.target.value)
    };

    const updateText = (newName: string) => {
        return new Promise((resolve) => {
            setName(newName);
            resolve()
        });
    };

    return (
        <>
            <Card clickHandler={handleClickOpen} index={index} card={card} />
            <MuiDialog onClose={handleClose} open={open} transitionDuration={0}>
                <DialogTitle id="card-dialog-title" onClose={handleClose}>
                    <InlineTextEdit classes={classes.dialogHeader} text={name} onUpdate={updateText} rowsMax={2} rowsMin={1} />
                </DialogTitle>

                <DialogContent dividers>
                    <h4>Description</h4>
                    <TextareaAutosize rowsMin={2} value={description} onChange={handleDescriptionChange}
                                      className={classes.textArr} placeholder="Add a more detailed description for this card..." />

                    <h4>Due date</h4>
                    <MuiPickersUtilsProvider utils={DateFnsUtils} >
                        <DatePicker
                            margin="normal"
                            id="date-picker-dialog"
                            label="Pick card due date"
                            format="MM/dd/yyyy"
                            value={dueDate}
                            onChange={handleDateChange}
                            KeyboardButtonProps={{'aria-label': 'change date'}}
                        />
                    </MuiPickersUtilsProvider>
                </DialogContent>

                <DialogActions>
                    <Button autoFocus onClick={saveChanges} color="primary">
                        Save changes
                    </Button>
                </DialogActions>
            </MuiDialog>
        </>
    );
}

const mapDispatchToProps = (dispatch: any) => ({
    updateCard: (card: CardType) => {
        return dispatch(cardActions.update(card))
    }
});

export default connect(null, mapDispatchToProps)(CardDialog);
