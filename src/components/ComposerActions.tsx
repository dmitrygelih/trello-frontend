import React from 'react';

import CloseIcon from '@material-ui/icons/Close';
import IconButton from '@material-ui/core/IconButton';
import FormButton from './FormButton'

import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
    iconButton: {
        '&:hover': {
            backgroundColor: 'transparent',
        },
    },
    closeIcon: {
        color: '#42526e',
        '&:hover': {
            color: '#192B4C'
        },
    },
}));

interface ComposerProps {
    text: string,
    submit: () => void,
    close: () => void
}

function ComposerActions({ text, submit, close }: ComposerProps) {
    const classes = useStyles();

    return (
        <div>
            <FormButton disableRipple disableElevation onClick={submit}>
                {text}
            </FormButton>
            <IconButton size="small" aria-label="close" onClick={close} className={classes.iconButton} disableRipple>
                <CloseIcon className={classes.closeIcon} />
            </IconButton>
        </div>
    )
}

export default ComposerActions;
