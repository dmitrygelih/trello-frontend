import React, {useRef, useState} from "react";
import useKeyboardKeypress from "../hooks/useKeyboardKeypress";
import useOutsideClick from "../hooks/useOutsideClick";
import ComposerActions from "./ComposerActions";
import {makeStyles} from "@material-ui/core/styles";
import TextareaAutosize from '@material-ui/core/TextareaAutosize';
import clsx from "clsx";

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        display: 'inline-block',
        backgroundColor: '#ebecf0',
        padding: 4,
        borderRadius: theme.shape.borderRadius
    },
    textArea: {
        padding: theme.spacing(3, 4, 1),
        border: 'none',
        height: 'auto',
        maxHeight: 162,
        resize: 'none',
        overflowY: 'auto',
        boxSizing: 'border-box',
        borderRadius: theme.shape.borderRadius,
        marginBottom: theme.spacing(2),
        boxShadow: '0 1px 0 rgba(9,30,66,.25)',
        width: '100%',
        '&:focus': {
            outline: 'none',
        }
    },
    nameInput: {
        color: '#192B4C',
        borderRadius: theme.shape.borderRadius,
        border: 'none',
        width: '100%',
        outline: 'none',
        height: 36,
        marginBottom: theme.spacing(2),
        padding: theme.spacing(4, 6),
        boxShadow: 'inset 0 0 0 2px #0079bf'
    },

}));

type StageFormProps = {
    submit: (value: string) => void,
    close: () => void,
    buttonText: string,
    placeholder: string,
    type: string,
    rows?: number,
    classes?: {}
}

function ComposerForm(props: StageFormProps) {
    const [name, setName] = useState('');
    const wrapperRef = useRef<HTMLDivElement>(null);
    const inputRef = useRef<any>(null);
    const classes = useStyles();

    useOutsideClick(wrapperRef, props.close);

    useKeyboardKeypress('Escape', props.close);

    const handleNameChange = (event: React.FormEvent) => {
        const target = event.target as HTMLTextAreaElement;
        if (target.value.trimRight() === '') { return }
        setName(target.value)
    };

    const handleSubmit = () => {
        if (inputRef && inputRef.current) {
            inputRef.current.focus();
            let value = inputRef.current.value;
            if (value === '') { return }

            props.submit(value.trim())
        }
    };

    const defineTextElement = () => {
        const inputElement = <input autoFocus ref={inputRef} value={name} onChange={handleNameChange}
                                    className={clsx(classes.nameInput, props.classes)} placeholder={props.placeholder} />;
        switch (props.type) {
            case 'input':
                return inputElement;
            case 'textArea':
                return (
                    <TextareaAutosize autoFocus ref={inputRef} value={name} rowsMin={props.rows || 4} onChange={handleNameChange}
                           className={clsx(classes.textArea, props.classes)} placeholder={props.placeholder} />
                );
            default:
                return inputElement
        }
    };

    useKeyboardKeypress('Enter', handleSubmit);

    return (
        <div className={classes.root} ref={wrapperRef}>
            {defineTextElement()}
            <ComposerActions submit={handleSubmit} close={props.close} text={props.buttonText} />
        </div>
    )
}

export default ComposerForm