import Button from "@material-ui/core/Button";
import { withStyles } from "@material-ui/core/styles";

const FormButton = withStyles({
    root: {
        color: '#fff',
        backgroundColor: '#5aac44',
        boxShadow: 'none',
        textTransform: 'none',
        height: '32px',
        padding: '6px 12px',
        border: 'none',
        lineHeight: 1.5,
        '&:hover': {
            backgroundColor: '#61bd4f',
        },
        '&:active': {
            boxShadow: 'none',
            backgroundColor: '#50843f',
        },
    },
})(Button);

export default FormButton
