import React, {useRef, useState} from 'react';

import TextareaAutosize from '@material-ui/core/TextareaAutosize';
import useKeyboardKeypress from "../hooks/useKeyboardKeypress";
import useOutsideClick from "../hooks/useOutsideClick";
import { makeStyles } from "@material-ui/core/styles";
import clsx from 'clsx'

const useStyles = makeStyles((theme) => ({
    activeTextArea: {
        padding: theme.spacing(2, 4),
        border: 'none',
        resize: 'none',
        fontWeight: 600,
        borderRadius: theme.shape.borderRadius,
        boxShadow: 'inset 0 0 0 2px #0079bf',
        width: '100%',
        '&:focus': {
            outline: 'none',
        }
    },
    textArea: {
        width: '100%',
        fontWeight: 600,
        boxShadow: 'none',
        padding: theme.spacing(2, 4),
        cursor: 'pointer',
        border: 'none',
        resize: 'none',
        borderRadius: theme.shape.borderRadius,
        textDecoration: 'none',
        background: 'none',
        '&:focus': {
            outline: 'none',
        }
    }
}));

type EditedTextProps = {
    text: string,
    submit: (text: string) => void,
    cancel: () => void,
    rowsMin?: number,
    rowsMax?: number,
    classes?: any
}

function EditedText({ text, submit, cancel, rowsMin, rowsMax, classes }: EditedTextProps) {
    const [localText, setLocalText] = useState(text || '');
    const inputRef = useRef<HTMLTextAreaElement>(null);
    const defaultClasses = useStyles();

    useOutsideClick(inputRef, submit);

    const onChangeHandler = (event: any) => {
        setLocalText(event.target.value)
    };

    const focusHandler = (event: any) => {
        event.target.select();
    };

    useKeyboardKeypress('Escape', cancel);

    useKeyboardKeypress('Enter', () => {
        if (inputRef && inputRef.current) {
            submit(inputRef.current.value)
        }
    });

    return (
        <TextareaAutosize autoFocus ref={inputRef} onFocus={focusHandler}
                          className={clsx(defaultClasses.activeTextArea, classes) }
                          onChange={onChangeHandler}
                          value={localText} />
    )
}

type InlineTextEditProps = {
    text: string,
    onUpdate: (text: string) => any,
    rowsMin?: number,
    rowsMax?: number,
    classes?: string
}

function InlineTextEdit({ text, onUpdate, rowsMax, rowsMin, classes }: InlineTextEditProps) {
    const [editActive, setEditActive] = useState(false);
    const defaultClasses = useStyles();

    const submit = (newText: string) => {
        if (text === newText) {
            setEditActive(false);
            return;
        }

        if (newText === '') {
            setEditActive(false);
            return;
        }

        onUpdate(newText).then(() => {
            setEditActive(false)
        })
    };

    const cancelEdit = () => {
        setEditActive(false)
    };

    const openEdit = () => {
        setEditActive(true)
    };

    return (
        <>{editActive
            ? <EditedText cancel={cancelEdit} submit={submit} text={text} classes={clsx(defaultClasses.activeTextArea, classes)} />
            : <TextareaAutosize onClick={openEdit} value={text} className={clsx(defaultClasses.textArea, classes)} />
          }</>
    )
}

export default InlineTextEdit;
