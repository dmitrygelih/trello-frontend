import React from 'react';

import ClearIcon from '@material-ui/icons/Clear';
import CardDialog from './CardDialog'
import CardComposer from './CardComposer'
import { Droppable } from "react-beautiful-dnd";

import StageType from '../types/stage'
import CardType from '../types/card'

import stageActions from "../store/actions/stage-actions";
import { connect } from "react-redux";
import InlineTextEdit from './InlineTextEdit'
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
    stageWrapper: {
        width: 272,
        margin: theme.spacing(0, 2),
        height: '100%',
        boxSizing: 'border-box',
        display: 'inline-block',
        verticalAlign: 'top',
        whiteSpace: 'nowrap',
        '&:first-child': {
            marginLeft: theme.spacing(4)
        }
    },
    list: {
        backgroundColor: '#ebecf0',
        borderRadius: theme.shape.borderRadius,
        boxSizing: 'border-box',
        display: 'flex',
        flexDirection: 'column',
        maxHeight: '100%',
        position: 'relative',
        overflow: 'hidden',
        whiteSpace: 'normal',
        padding: 8
    },
    header: {
        display: 'flex',
        marginBottom: theme.spacing(4),
        justifyContent: 'space-between',
        minHeight: 28,
        alignItems: 'center',
        wordBreak: 'break-all',
        lineHeight: 'normal',
    },
    deleteIcon: {
        height: 28,
        padding: 2,
        cursor: 'pointer',
        '&:hover': {
            background: '#D9DBE2',
            color: '#192B4C',
        }
    },
    cardList: {
        flex: '1 1 auto',
        overflowY: 'auto',
        overflowX: 'hidden',
        minHeight: 1,
    }
}));

type StageProps = {
    stage: StageType,
    cards: CardType[],
    deleteStage: (stageId: string | number) => void,
    updateStage: (stage: StageType) => Promise<any>
}

function Stage({ stage, cards, deleteStage, updateStage }: StageProps) {
    const classes = useStyles();

    const remove = () => {
        deleteStage(stage.id)
    };

    const updateName = (newName: string):Promise<StageType> => {
        return updateStage({ ...stage, name: newName });
    };

    return (
        <Droppable droppableId={String(stage.id)}>
            {(provided, snapshot) => (
                <div className={classes.stageWrapper}>
                    <div className={classes.list}>
                        <div className={classes.header}>
                            <InlineTextEdit text={stage.name} onUpdate={updateName} rowsMax={3} rowsMin={1} />
                            <span className={classes.deleteIcon} onClick={remove}>
                                <ClearIcon />
                            </span>
                        </div>

                        <div className={classes.cardList} {...provided.droppableProps} ref={provided.innerRef}>
                            {stage.cardIds.length > 0 &&
                                stage.cardIds.map((cardId:any, index: number) => {
                                    const card = cards[cardId];
                                    return <CardDialog card={card} key={card.id} index={index} />
                                })
                            }
                            {provided.placeholder}
                        </div>
                        <CardComposer stageId={stage.id}/>
                    </div>
                </div>
            )}
        </Droppable>
    )
}

const mapStateToProps = (state: any) => ({
    cards: state.cards.data,
});

const mapDispatchToProps = (dispatch: any) => ({
    deleteStage: (stageId: number | string) => {
        dispatch(stageActions.delete(stageId))
    },
    updateStage: (stage: StageType) => {
        return dispatch(stageActions.update(stage))
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(Stage);
