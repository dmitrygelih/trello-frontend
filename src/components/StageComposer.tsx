import React, { useState } from 'react';

import stageActions from "../store/actions/stage-actions";
import { connect } from "react-redux";
import Button from '@material-ui/core/Button';
import ComposerForm from './ComposerForm'
import AddIcon from '@material-ui/icons/Add';
import { makeStyles } from "@material-ui/core/styles";
import StageType from "../types/stage";

const useStyles = makeStyles((theme) => ({
    root: {
        marginRight: theme.spacing(4),
        marginLeft: theme.spacing(2),
        width: 272,
        display: 'inline-block',
        '&:first-child': {
            marginLeft: theme.spacing(4)
        }
    },
    openFormButton: {
        justifyContent: 'left',
        height: 40,
        padding: '10px, 12px',
        '&:hover': {
            backgroundColor: '#66A4D2'
        }
    },
}));

interface StageComposerProps {
    createStage: ({}: any) => void,
    stages: StageType[],
    boardId: number
}

function StageComposer(props: StageComposerProps) {
    const [formActive, setFormActive] = useState(false);
    const classes = useStyles();

    const openForm = () => {
        setFormActive(true)
    };

    const closeForm = () => {
        setFormActive(false)
    };

    const createStage = (stageName: string) => {
        props.createStage({ name: stageName, board_id: props.boardId });
        setFormActive(false)
    };

    const formProps = {
        type: "input",
        buttonText: 'Add Stage',
        close: closeForm,
        submit: createStage,
        placeholder: 'Enter stage title...',
    };

    return (
        <div className={classes.root}>
            {formActive
                ? <ComposerForm {...formProps} />
                : <Button variant="contained" color="secondary"
                          disableElevation onClick={openForm} fullWidth
                          className={classes.openFormButton}>
                    <AddIcon fontSize='small'/> Add another stage
                  </Button>
            }
        </div>
    )
}

const mapStateToProps = (state: any) => ({
    stages: state.stages.data,
});

const mapDispatchToProps = (dispatch: any) => ({
    createStage: (stage: StageType) => {
        dispatch(stageActions.create(stage))
    }
});


export default connect(mapStateToProps, mapDispatchToProps)(StageComposer);
