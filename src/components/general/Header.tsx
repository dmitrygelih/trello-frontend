import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Dashboard from '@material-ui/icons/Dashboard';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import { useHistory } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
    appBar: {
        height: 40,
        marginBottom: theme.spacing(4),
        boxShadow: 'none'
    },
    toolBar: {
        minHeight: 40,
        paddingLeft: theme.spacing(4),
        paddingRight: theme.spacing(4)
    },
    icon: {
        marginRight: theme.spacing(4),
    },
}));

const Header = () => {
    const classes = useStyles();
    const history = useHistory();

    const toHome = () => {
        history.push('/')
    };

    return (
        <>
            <AppBar position="relative" className={classes.appBar}>
                <Toolbar className={classes.toolBar}>
                    <Dashboard className={classes.icon} onClick={toHome} />
                    <Typography variant="h6" color="inherit" noWrap>
                        Mini-trello
                    </Typography>
                </Toolbar>
            </AppBar>
        </>
    );
};

export default Header;
