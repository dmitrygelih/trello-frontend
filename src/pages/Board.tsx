import React, { useEffect } from 'react';
import { useParams } from "react-router-dom"
import { connect, batch } from "react-redux";
import { DragDropContext, DropResult } from "react-beautiful-dnd";
import axios from 'axios';

import Header from '../components/general/Header'

import {CARDS_STORE, STAGE_UPDATE, STAGES_STORE, LOADING_SHOW, LOADING_HIDE} from '../store/action-types'

import CardType from '../types/card'
import StageType from '../types/stage'

import cardActions from "../store/actions/card-actions";
import stageActions from "../store/actions/stage-actions";
import StageComposer from '../components/StageComposer'
import Stage from '../components/Stage';

import { isObjectEmpty } from '../utils/object-utils'
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        flexDirection: 'column',
        height: '100%',
    },
    popoverBoundary: {
        overflowY: 'auto',
        display: 'flex',
        flexDirection: 'column',
        flexGrow: 1,
    },
    content: {
        position: 'relative',
        overflowY: 'auto',
        outline: 'none',
        flexGrow: 1,
    },
    boardWrapper: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
    },
    boardMarginContent: {
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
        marginRight: 0,
        position: 'relative',
        transition: 'margin .1s ease-in'
    },
    boardCanvas: {
        position: 'relative',
        flexGrow: 1,
    },
    board: {
        userSelect: 'none',
        whiteSpace: 'nowrap',
        overflowX: 'auto',
        overflowY: 'hidden',
        paddingBottom: theme.spacing(4),
        position: 'absolute',
        top: 0,
        right: 0,
        bottom: 0,
        left: 0,
    }
}));

interface BoardProps {
    stages: { [id: string]: StageType },
    loading: boolean,
    storeCards: (cards: CardType[]) => void,
    storeStages: (stages: StageType[]) => void,
    showLoading: () => void,
    hideLoading: () => void,
    updateStageCards: (stage: StageType) => void,
    updateCardPosition: (draggableId: string, droppableId: string, index: number) => void,
}

const Board = (props: BoardProps) => {
    let { id } = useParams();
    const classes = useStyles();

    useEffect(() => {
        props.showLoading();

        axios.get(`api/boards/${id}`)
            .then((response) => {
                batch(() => {
                    props.storeCards(response.data.cards);
                    props.storeStages(response.data.stages);
                })
            })
            .finally(() => {
                props.hideLoading();
            })
    }, []);

    const sourceSameHandler = (result: DropResult) => {
        const { source, destination, draggableId } = result;

        const stage = props.stages[source.droppableId];
        const newCardIds = Array.from(stage.cardIds);

        newCardIds.splice(source.index, 1);
        newCardIds.splice(destination.index, 0, draggableId);

        const newStage = {
            ...stage,
            cardIds: newCardIds
        };

        props.updateStageCards(newStage);
        props.updateCardPosition(draggableId, destination.droppableId, destination.index)
    };

    const sourceDifferentHandler = (result: DropResult) => {
        const { source, destination, draggableId } = result;

        const startStage = props.stages[source.droppableId];
        const finishStage = props.stages[destination.droppableId];

        const sourceCardIds = Array.from(startStage.cardIds);
        const destinationCardIds = Array.from(finishStage.cardIds);

        sourceCardIds.splice(source.index, 1);
        destinationCardIds.splice(destination.index, 0, draggableId);

        const newSourceStage = {
            ...startStage,
            cardIds: sourceCardIds
        };

        const newDestinationStage = {
            ...finishStage,
            cardIds: destinationCardIds
        };

        batch(() => {
            props.updateStageCards(newSourceStage);
            props.updateStageCards(newDestinationStage);
        });

        props.updateCardPosition(draggableId, destination.droppableId, destination.index)
    };

    const onDragEndHandler = (result: DropResult) => {
        if (!result.destination) { return }

        const isDestinationSourceSame = result.destination.droppableId === result.source.droppableId;
        const isSourceIndexSame = result.destination.index === result.source.index;
        const isDestinationSame = isDestinationSourceSame && isSourceIndexSame;

        if (isDestinationSame) { return }

        isDestinationSourceSame ? sourceSameHandler(result) : sourceDifferentHandler(result)
    };

    return (
            <div className={classes.root}>
                <Header />
                <div className={classes.popoverBoundary}>
                    <div className={classes.content}>
                        <div className={classes.boardWrapper}>
                            <div className={classes.boardMarginContent}>
                                <div className={classes.boardCanvas}>
                                    <DragDropContext onDragEnd={onDragEndHandler}>
                                        <div className={classes.board}>
                                            {(!isObjectEmpty(props.stages) && !props.loading) &&
                                                Object.keys(props.stages).map((stageId: string) => {
                                                    return <Stage stage={props.stages[stageId]} key={stageId} />
                                                })
                                            }
                                            <StageComposer boardId={id} />
                                        </div>
                                    </DragDropContext>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    );
};

const mapStateToProps = (state: any) => ({
    stages: state.stages.data,
    loading: state.pageLoading
});

const mapDispatchToProps = (dispatch: any) => ({
    updateStageCards: (stage: StageType) => {
        dispatch({ type: STAGE_UPDATE, stage })
    },
    updateStage: (stage: StageType) => {
        return dispatch(stageActions.update(stage))
    },
    storeStages: (stages: StageType[]) => {
        dispatch({ type: STAGES_STORE, stages })
    },
    storeCards: (cards: CardType[]) => {
        dispatch({ type: CARDS_STORE, cards })
    },
    updateCardPosition: (cardId: string | number, stageId: string | number, position: number) => {
        return dispatch(cardActions.updateCardPosition(cardId, stageId, position))
    },
    showLoading: () => { dispatch({ type: LOADING_SHOW }) },
    hideLoading: () => { dispatch({ type: LOADING_HIDE }) },
});

export default connect(mapStateToProps, mapDispatchToProps)(Board);
