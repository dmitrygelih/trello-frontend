import React, { useEffect, useState } from 'react';

import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { useHistory } from "react-router-dom";
import { connect } from 'react-redux';

import axios from 'axios'
import { LOADING_HIDE, LOADING_SHOW } from "../store/action-types";

import Header from '../components/general/Header'
import BoardDialog from '../components/BoardDialog'
import Board from '../types/board'

const useStyles = makeStyles((theme) => ({
    card: {
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
    },
    cardContent: {
        flexGrow: 1,
    },
}));

interface HomeProps {
    showLoading: () => void;
    hideLoading: () => void;
}

const Home = ({ showLoading, hideLoading }: HomeProps) => {
    const classes = useStyles();
    const history = useHistory();
    const [boards, setBoards] = useState<Array<Board>>([]);

    useEffect(() => {
        showLoading();

        axios.get('api/boards')
            .then(response => { setBoards(response.data) })
            .finally(() => { hideLoading() })
    }, []);

    const toBoard = (boardId: number | string) => {
        history.push(`/boards/${boardId}`)
    };

    const addBoard = (newBoard: Board) => {
        setBoards([...boards, newBoard])
    };

    return (
        <>
            <Header />
            <Container maxWidth="md" disableGutters={true}>

                <BoardDialog title='Create new board' onCreate={addBoard} />
                <Grid container spacing={10}>
                    {
                        boards.map((board: Board) => (
                            <Grid item key={board.id} xs={12} sm={6} md={4}>
                                <Card className={classes.card}>
                                    <CardContent className={classes.cardContent}>
                                        <Typography>
                                            {board.name}
                                        </Typography>
                                    </CardContent>
                                    <CardActions>
                                        <Button size="small" color="primary" onClick={() => toBoard(board.id)}>
                                            View
                                        </Button>
                                    </CardActions>
                                </Card>
                            </Grid>
                        ))
                    }
                </Grid>
            </Container>
        </>
    );
};

const mapDispatchToProps = (dispatch: any) => ({
    showLoading: () => { dispatch({ type: LOADING_SHOW }) },
    hideLoading: () => { dispatch({ type: LOADING_HIDE }) },
});

export default connect(null, mapDispatchToProps)(Home);
