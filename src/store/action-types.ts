// Stages
export const STAGES_STORE = 'STAGES_STORE';
export const STAGE_DELETE = 'STAGE_DELETE';
export const STAGE_ADD = 'STAGE_ADD';
export const STAGE_ADD_CARD_ID = 'STAGE_ADD_CARD_ID';
export const STAGE_UPDATE = 'STAGE_UPDATE';

// Cards
export const CARDS_STORE = 'CARDS_STORE';
export const CARD_ADD = 'CARD_ADD';
export const CARD_UPDATE = 'CARD_UPDATE';

// Loading
export const LOADING_HIDE = 'LOADING_HIDE';
export const LOADING_SHOW = 'LOADING_SHOW';