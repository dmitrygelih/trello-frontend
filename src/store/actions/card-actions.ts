import { CARD_UPDATE, STAGE_ADD_CARD_ID, CARD_ADD } from '../action-types'
import Card from '../../types/card'
import axios from "axios";
import { batch } from 'react-redux'

const cardActions = {
    create: (card: { name: string, stageId: string | number }) => {
        return (dispatch: any) => {
            return new Promise((resolve, reject) => {
                axios.post('api/cards', card)
                    .then(response => {
                        const card = response.data.card;
                        batch(() => {
                            dispatch({ type: CARD_ADD, card });
                            dispatch({ type: STAGE_ADD_CARD_ID, cardId: card.id, stageId: card.stageId });
                        });

                        resolve(response.data)
                    })
                    .catch(error => reject(error))
            })
        }
    },
    updateCardPosition: (cardId: any, stageId: any, position: any) => {
        return (dispatch: any) => {
            return new Promise((resolve, reject) => {
                const requestParams = { id: cardId, stageId, position };

                axios.put(`api/cards/${cardId}/update_position`, requestParams)
                    .then(response => {
                        resolve(response)
                    })
                    .catch(error => reject(error))
            })
        }
    },
    update: (card: Card) => {
        return (dispatch: any) => {
            return new Promise((resolve, reject) => {
                axios.put(`api/cards/${card.id}`, card)
                    .then(response => {
                        dispatch({ type: CARD_UPDATE, card: response.data.card });
                        resolve(response)
                    })
                    .catch(error => reject(error))
            })
        }
    },
};

export default cardActions