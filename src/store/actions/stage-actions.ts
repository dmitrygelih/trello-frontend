import { STAGE_UPDATE, STAGE_DELETE, STAGE_ADD } from '../action-types'
import Stage from '../../types/stage'
import axios from "axios";

const stageActions = {
    update: (stage: Stage) => {
        return (dispatch: any) => {
            return new Promise((resolve, reject) => {
                axios.put(`api/stages/${stage.id}`, stage)
                    .then(response => {
                        dispatch({ type: STAGE_UPDATE, stage: response.data.stage });
                        resolve(response.data.stage)
                    })
                    .catch(error => reject(error))
            })
        }
    },
    delete: (stageId: number | string) => {
        return (dispatch: any) => {
            axios.delete(`api/stages/${stageId}`)
                .then(response => dispatch({ type: STAGE_DELETE, stageId }) )
        }
    },
    create: (stage: Stage) => {
        return (dispatch: any) => {
            axios.post('api/stages', stage)
                .then(response => {
                    let stage = response.data.stage;
                    stage.cardIds = [];
                    dispatch({ type: STAGE_ADD, stage })
                })
        }
    },
};

export default stageActions
