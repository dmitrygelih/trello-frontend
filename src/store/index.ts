import { createStore, applyMiddleware } from 'redux'

import thunk from 'redux-thunk';
import logger from 'redux-logger'

import reducers from './reducers'

const middleware = applyMiddleware(thunk, logger);
const store = createStore(reducers, middleware);

export type AppDispatch = typeof store.dispatch

export default store;
