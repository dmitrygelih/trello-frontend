import { CARDS_STORE, CARD_ADD, CARD_UPDATE } from '../action-types'
import update from 'immutability-helper';
import Card from "../../types/card";

interface stateObject {
    data: {
        [key: string]: Card;
    }
}

const initialState: stateObject = {
    data: {}
} ;

const cardsReducer = (state = initialState, action: any) => {
    let newData;
    switch (action.type) {
        case CARDS_STORE:
            if (!action.cards) { action.cards = {} }

            return {
                ...state,
                data: action.cards
            };
        case CARD_ADD:
            newData = update(state.data, {
                [action.card.id]: {$set: action.card}
            });

            return {
                ...state,
                data: newData
            };
        case CARD_UPDATE:
            newData = update(state.data, {
                [action.card.id]: {$set: action.card}
            });

            return {
                ...state,
                data: newData
            };
        default:
            return state
    }
};

export default cardsReducer
