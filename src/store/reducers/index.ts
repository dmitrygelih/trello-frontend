import { combineReducers } from 'redux'

import cards from './cards-reducer'
import stages from './stages-reducer'
import pageLoading from './page-loading-reducer'

export default combineReducers({
    cards,
    stages,
    pageLoading
})