import { LOADING_HIDE, LOADING_SHOW } from '../action-types'

interface ActionType {
    type: string
}

const initialState: boolean = false;

const pageLoadingReducer = (state = initialState, action: ActionType) => {
    switch (action.type) {
        case LOADING_SHOW:
            return true;
        case LOADING_HIDE:
            return false;
        default:
            return state
    }
};

export default pageLoadingReducer
