import { STAGES_STORE, STAGE_ADD, STAGE_ADD_CARD_ID, STAGE_DELETE, STAGE_UPDATE } from '../action-types'
import update from 'immutability-helper';
import State from '../../types/stage'

interface stateObject {
    data: {
        [key: string]: State;
    }
}

const initialState: stateObject = {
    data: {}
} ;

const stagesReducer = (state = initialState, action: any) => {
    let newData;
    switch (action.type) {
        case STAGES_STORE:
            if (!action.stages) { action.stages = {} }

            return {
                ...state,
                data: action.stages
            };
        case STAGE_DELETE:
            newData = { ...state.data };
            delete newData[action.stageId];

            return {
                ...state,
                data: newData
            };
        case STAGE_ADD:
            newData = {
                ...state.data,
                [action.stage.id]: action.stage
            };

            return {
                data: newData
            };
        case STAGE_ADD_CARD_ID:
            const stage = state.data[action.stageId];
            const newCardIds = update(stage.cardIds, { $push: [action.cardId] });

            newData = {
                ...state.data,
                [action.stageId]: { ...stage, ['cardIds']: newCardIds }
            };

            return {
                ...state,
                data: newData
            };
        case STAGE_UPDATE:
            const updatedStage = update(state.data[action.stage.id], {$merge: action.stage});

            return {
                ...state,
                data: {
                    ...state.data,
                    [action.stage.id]: updatedStage
                }
            };
        default:
            return state
    }
};

export default stagesReducer
