import { createMuiTheme } from '@material-ui/core/styles';
import 'fontsource-roboto';

const theme = createMuiTheme({
    spacing: 2,
    overrides: {
        MuiCssBaseline: {
            '@global': {
                html: {
                    WebkitFontSmoothing: 'auto',
                    height: '100%'
                },
                body: {
                    fontFamily: [ 'Roboto' ].join(','),
                    fontSize: 14,
                    fontWeight: 400,
                    color: '#172b4d',
                    height: '100%',
                    margin: 0,
                    position: 'relative',
                    zIndex: 0,
                    overflow: 'hidden'
                },
                '#root': {
                    height: '100%',
                    position: 'relative',
                    zIndex: 1,
                },
                textArea: {
                    color: '#172b4d',
                    fontFamily: [ 'Roboto' ].join(','),
                },
            },
        },
        MuiPaper: {
            root: {
                backgroundColor: '#F4F5F7',
                color: '#172b4d',
            }
        },
    },
    shape: {
        borderRadius: 3,
    },
    typography: {
        button: {
            textTransform: 'none'
        }
    },
    palette: {
        primary: {
            main: '#1967A0',
            contrastText: '#FFF',
        },
        secondary: {
            main: '#5A97C8',
            contrastText: '#FFF',
        },
        info: {
            main: '#5A97C8',
            contrastText: '#FFF'
        },
        background: {
            default: '#1d79bc'
        }
    },
});

export default theme