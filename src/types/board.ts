type BoardType = {
    id: number | string,
    name: string
}

export default BoardType
