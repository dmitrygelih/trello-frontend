type CardType = {
    id: number | string,
    name: string,
    stageId: number
    description?: string,
    dueDate?: string | Date,
}

export default CardType
