type StageType = {
    id: number | string,
    name: string,
    cardIds: string[],
}

export default StageType
