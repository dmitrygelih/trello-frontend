const ONE_DAY = 1000 * 60 * 60 * 24;

export function daysTillDate(dueDate: Date) {
    const differenceInMs = dueDate.getTime() - new Date().getTime();
    return Math.round(differenceInMs / ONE_DAY);
}

